package com.basic.basicapp

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class BasicAppApplication

fun main(args: Array<String>) {
    runApplication<BasicAppApplication>(*args)
}
